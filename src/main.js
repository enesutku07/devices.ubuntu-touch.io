// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

const slugify = require("@sindresorhus/slugify");

import DefaultLayout from "~/layouts/Default.vue";

import "~/assets/scss/main.scss";

export default function(Vue, { router, head, isClient }) {
  Vue.component("Layout", DefaultLayout);

  router.beforeResolve((to, from, next) => {
    // do not rewrite build paths
    if (process.isServer) {
      return next();
    }

    let destPath = to.fullPath.split("/");
    let lastElementInPath = destPath.pop();
    lastElementInPath = lastElementInPath ? lastElementInPath : destPath.pop();
    if (
      lastElementInPath != slugify(lastElementInPath, { decamelize: false }) &&
      destPath.pop() == "device"
    ) {
      next({
        path:
          "/device/" + slugify(lastElementInPath, { decamelize: false }) + "/"
      });
    }
    return next();
  });

  // Set metadata
  head.meta.push({
    property: "og:type",
    content: "website"
  });
  head.meta.push({
    property: "og:image",
    content: "https://devices.ubuntu-touch.io/social-preview.jpg"
  });
}
